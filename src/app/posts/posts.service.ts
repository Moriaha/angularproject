import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';

@Injectable()
export class PostsService {
 //private _url ='http://jsonplaceholder.typicode.com/posts';

postsObservable;

getPosts(){
//return this._http.get(this._url).map(res => res.json()).delay(2000);
this.postsObservable=this.af.database.list('/posts').map(
posts => {
  posts.map(
    post => {
      post.userName =[];
      for (var p in post.users){
        post.userName.push(
          this.af.database.object('/users/' + p)
        )
      }
    }

  );
  return posts;

}

)

return this.postsObservable;
}

addPosts(post){
  this.postsObservable.push(post);
}

updatePost(post){
let postKey= post.$key;
let postData= {title:post.title , content:post.content};
this.af.database.object('/posts/' + postKey).update(postData);
}

deletPost(post){
let postKey = post.$key;
this.af.database.object('/posts/' + postKey).remove();

}

/*getPosts(){
let posts = [
  {title:'A', content:'AAAAAA', author:'John'},
  {title:'B', content:'BBBBBB', author:'Moria'},
  {title:'C', content:'CCCCCC', author:'Eli'},
  {title:'D', content:'DDDDDD', author:'Jack'},
]
 return posts;
}*/

  constructor(private af:AngularFire) { }

}
