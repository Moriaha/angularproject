import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  //styleUrls: ['./posts.component.css'],
   styles: [`
   .posts li { cursor: default; } 
   .posts li:hover { background: #ecf0f1; }  
   .list-group-item.active,
   .list-group-item.active:hover { 
     background-color: #ecf0f1;
     border-color: #ecf0f1; 
      color: #2c3e50;}
  `]
})



export class PostsComponent implements OnInit {

posts;
currentPost;
isLoading=true;

  select(post){
		this.currentPost = post; 
    console.log(	this.currentPost);
 }

addPost(post){
  // this.posts.push(post)
   this._postService.addPosts(post);
 }

 editUser(post){
  //this.posts[this.posts.indexOf(post)].title=post.title;
  //this.posts[this.posts.indexOf(post)].content=post.content;
  this._postService.updatePost(post);
 }

  deleteUser(post){
  //this.posts.splice(this.posts.indexOf(post),1)
  this._postService.deletPost(post);
}

  constructor(private _postService:PostsService) { }

  ngOnInit() {
    // this.posts = this._postService.getPosts();
     this._postService.getPosts().subscribe(postsData => {this.posts=postsData; this.isLoading=false});
  }

}
