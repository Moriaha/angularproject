import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/delay';


@Injectable()
export class UsersService {
 private _url ='http://jsonplaceholder.typicode.com/users';

//method to recieve users from URL
getUsers(){
return this._http.get(this._url).map(res => res.json()).delay(2000);
}
  constructor(private _http:Http) { }

}