import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
   styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }  
  `]
})
export class UsersComponent implements OnInit {

  users;
  isLoading=true;
  currentUser;

  select(user){
		this.currentUser = user; 
    console.log(	this.currentUser);
 }

 addUser(user){
   this.users.push(user)
 }


  constructor(private _userService:UsersService) { }

deleteUser(user){
  this.users.splice(this.users.indexOf(user),1)
}

  ngOnInit() {
    this._userService.getUsers().subscribe(usersData =>{this.users=usersData; this.isLoading=false});

  }

}
